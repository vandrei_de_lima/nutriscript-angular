import { Calc } from './type';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  calc: Calc = {
    activityLevel: 'low',
    goal: null,
    carbohydrate: 2,
    weight: null,
    gender: null,
    height: null,
    protein: 2,
    age: null,
    fat: 0,
  };

  activityLevelList = {
    masc: { veryLow: 1.05, low: 1.55, moderate: 1.78, height: 2.1 },
    fem: { veryLow: 1.05, low: 1.56, moderate: 1.64, height: 1.82 },
  };

  totalCaloricExpenditure: number = 0;
  basalMetabolicRate: number = 0;
  kclForObjective: number = 0;

  isStartCalc: boolean = false;
  openFullCalc: boolean = false;

  goalProtein: number = 0;
  goalCarbohydrate: number = 0;
  goalFat: number = 0;

  showToast: boolean = false;
  messageError: string = '';
  startEffectButtonCalculator: boolean = false;

  constructor() {}

  selectedGender(gender): void {
    this.calc.gender = gender;
  }
  selectedGoal(activityLevel): void {
    this.calc.goal = activityLevel;
  }

  someProtein(option: boolean) {
    let valuePositive = this.calc.protein >= 0;

    if (option && valuePositive) {
      this.calc.protein += 0.1;
    } else if (valuePositive) {
      this.calc.protein -= 0.1;

      this.calcMacronutrients();
    }
  }

  someCarbohydrate(option: boolean) {
    let valuePositive = this.calc.carbohydrate >= 0;

    if (option && valuePositive) {
      this.calc.carbohydrate += 0.1;
    } else if (valuePositive) {
      this.calc.carbohydrate -= 0.1;
    }

    this.calcMacronutrients();
  }

  startCalc() {
    if (!this.verifyInput()) return (this.isStartCalc = true);
    this.openFullCalc = true;

    this.basalMetabolicRate = this.getBasalMetabolicRate();
    this.totalCaloricExpenditure = this.getTotalCaloricExpenditure();
    this.kclForObjective = 0;

    switch (this.calc.goal) {
      case 'lose':
        this.kclForObjective = this.totalCaloricExpenditure * 0.85;
        break;
      case 'keep':
        this.kclForObjective = this.totalCaloricExpenditure * 0.98;
        break;
      case 'win':
        this.kclForObjective = this.totalCaloricExpenditure * 1.2;
        break;
    }

    this.calcMacronutrients();
  }

  calcMacronutrients(): void {
    let weight = this.calc.weight;
    let gramsOfProtein = this.calc.protein;
    let gramsOfCarbohydrate = this.calc.carbohydrate;

    this.goalProtein = gramsOfProtein * weight;
    this.goalCarbohydrate = gramsOfCarbohydrate * weight;

    this.goalFat = this.getGramsOfFat();
  }

  getGramsOfFat(): number {
    let calcCarbProten = (this.goalProtein + this.goalCarbohydrate) * 4;

    let leftover = this.kclForObjective - calcCarbProten;

    return leftover / 9;
  }

  getTotalCaloricExpenditure(): number {
    let gender = this.calc.gender;
    let activityLevel = this.calc.activityLevel;

    let valueActivity = this.activityLevelList[gender][activityLevel];

    return this.basalMetabolicRate * valueActivity;
  }

  getBasalMetabolicRate(): number {
    let brm = 0;
    let age = this.calc.age;
    let weight = this.calc.weight;
    let height = this.calc.height;
    if (this.calc.gender === 'masc') {
      brm = 66 + 13.7 * weight + 5 * height - (6.8 + age);
    } else {
      brm = 665 + 9.6 * weight + 1.8 * height - (4.7 + age);
    }

    return brm;
  }

  verifyInput(): boolean {
    let isOk = true;

    if (!this.calc.age) {
      isOk = false;
    }
    if (!this.calc.weight) {
      isOk = false;
    }
    if (!this.calc.height) {
      isOk = false;
    }
    if (!this.calc.gender) {
      isOk = false;
    }

    return isOk;
  }

  checkField(field) {
    let isInvalid = false;

    if (!this.calc[field]) isInvalid = true;

    return isInvalid;
  }

  message(message) {
    this.showToast = true;
    this.messageError = message;
    setTimeout(() => {
      this.showToast = false;
    }, 3000);
  }
}
