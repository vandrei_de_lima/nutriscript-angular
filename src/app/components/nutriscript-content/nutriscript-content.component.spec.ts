import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutriscriptContentComponent } from './nutriscript-content.component';

describe('NutriscriptContentComponent', () => {
  let component: NutriscriptContentComponent;
  let fixture: ComponentFixture<NutriscriptContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutriscriptContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutriscriptContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
