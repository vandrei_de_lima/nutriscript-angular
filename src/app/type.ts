export class Calc {
  weight: number;
  gender: string;
  height: number;
  goal: string;
  age: number;
  activityLevel: string;
  protein: number;
  carbohydrate: number;
  fat: number;
}
