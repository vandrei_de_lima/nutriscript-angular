import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { HttpClientModule } from '@angular/common/http';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { ToastAlertComponent } from './components/interface/toast-alert/toast-alert.component';
import { NutriscriptContentComponent } from './components/nutriscript-content/nutriscript-content.component';
import { FooterComponent } from './components/interface/footer/footer.component';

@NgModule({
  declarations: [AppComponent, ToastAlertComponent, NutriscriptContentComponent, FooterComponent],
  imports: [
    BrowserModule,
    FormsModule,
    NzIconModule,
    NzButtonModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
